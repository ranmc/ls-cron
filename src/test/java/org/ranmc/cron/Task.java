package org.ranmc.cron;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author Eric Ran
 * @since 2021/1/21
 */
public class Task implements LsDelayed {

    /**
     * 任务名称
     */
    private final String name;

    /**
     * 执行时间戳
     */
    private final Long timestamp;

    public Task(String name, Long timestamp) {
        this.name = name;
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    /**
     * 返回值为<1开始执行
     *
     * @param unit 时间单位
     * @return <1 执行
     */
    public long getDelay(TimeUnit unit) {
        return timestamp - System.currentTimeMillis();
    }

    public int compareTo(Delayed o) {
        return Long.compare(getDelay(TimeUnit.SECONDS), o.getDelay(TimeUnit.SECONDS));
    }

    /**
     * 线程池执行
     */
    @Override
    public void run() {
        System.out.println(getName());
    }
}
