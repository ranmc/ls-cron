package org.ranmc.cron;

import org.junit.Test;
import org.ranmc.cron.impl.CronDelayQueue;

import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Eric Ran
 * @since 2021/1/21
 */
public class LsCronTest {

    @Test
    public void test() {
        Task task1 = new Task("任务一", System.currentTimeMillis() + 1000);
        Task task2 = new Task("任务二", System.currentTimeMillis() + 2000);
        Task task3 = new Task("任务三", System.currentTimeMillis() + 3000);
        Task task4 = new Task("任务四", System.currentTimeMillis() + 1500);
        LsCron<Task> cron = new CronDelayQueue<Task>()
                // 初始化任务
                .init(() -> Arrays.asList(task1, task2, task3))
                // 设置执行线程池
                .setExecutor(new ThreadPoolExecutor(1, 2, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>()))
                // 部署任务队列，开始执行
                .deploy()
                // 添加任务
                .add(task4)
                // 移除任务
                .remove(task3);
        // 停止任务
        cron.shutdown();
        // 阻塞主线程，实际业务中不需要
        cron.block();
    }

}