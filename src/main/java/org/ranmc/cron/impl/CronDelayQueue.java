package org.ranmc.cron.impl;

import org.ranmc.cron.LsCronInit;
import org.ranmc.cron.LsCron;
import org.ranmc.cron.LsDelayed;

import java.util.List;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Executor;

/**
 * 延时队列实现定时任务
 *
 * @author Eric Ran
 * @since 2021/1/21
 */
public class CronDelayQueue<T extends LsDelayed> implements LsCron<T> {

    private DelayQueue<T> queue;

    private Executor executor;

    private Thread main;

    private boolean run = true;

    @Override
    public LsCron<T> init(LsCronInit<List<T>> init) {
        queue = new DelayQueue<>(init.init());
        return this;
    }

    @Override
    public LsCron<T> setExecutor(Executor executor) {
        this.executor = executor;
        return this;
    }

    @Override
    public LsCron<T> deploy() {
        main = new Thread(() -> {
            while (run) {
                T t = queue.poll();
                if (t != null) {
                    executor.execute(t);
                }
            }
        });
        main.start();
        return this;
    }

    @SuppressWarnings("all")
    @Override
    public void block() {
        while (true) ;
    }

    @Override
    public void shutdown() {
        main.interrupt();
        run = false;
    }

    @Override
    public LsCron<T> add(T t) {
        queue.add(t);
        return this;
    }

    @Override
    public LsCron<T> remove(T t) {
        queue.remove(t);
        return this;
    }
}