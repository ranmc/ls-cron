package org.ranmc.cron;

/**
 * @author Eric Ran
 * @since 2021/1/21
 */
public interface LsCronInit<T> {

    T init();

}
