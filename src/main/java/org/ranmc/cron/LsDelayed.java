package org.ranmc.cron;

import java.util.concurrent.Delayed;

/**
 * @author Eric Ran
 * @since 2021/1/21
 */
public interface LsDelayed extends Delayed, Runnable {
}
