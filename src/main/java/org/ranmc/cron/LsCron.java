package org.ranmc.cron;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * 定时任务
 *
 * @author Eric Ran
 * @since 2021/1/21
 */
public interface LsCron<T> {

    /**
     * 初始化数据
     *
     * @param init 初始化函数
     * @return 调用链
     */
    LsCron<T> init(LsCronInit<List<T>> init);

    /**
     * 设置执行线程池
     *
     * @param executor 线程池
     * @return 调用链
     */
    LsCron<T> setExecutor(Executor executor);

    /**
     * 部署任务
     */
    LsCron<T> deploy();

    /**
     * 阻塞主线程
     */
    void block();

    /**
     * 停止任务
     */
    void shutdown();

    /**
     * 添加任务
     *
     * @param t 任务
     * @return 调用链
     */
    LsCron<T> add(T t);

    /**
     * 删除任务
     *
     * @param t 任务
     * @return 调用链
     */
    LsCron<T> remove(T t);

}
